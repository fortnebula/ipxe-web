# Virtual Machine based version of IPXE-WEB

This repo builds ipxe-web from https://github.com/xbgmsharp/ipxe-buildweb into a bootable virtual machine image. 

## How to use this image

This image includes cloud-init, so you may use cloud-init to configure anything you want to inside the image on boot. Things like user accounts and scripts may be run from the cloud-init interface. See https://cloudinit.readthedocs.io/en/latest/ for more information

## Download the image

The image can be obtained from here 

https://api.us-east.open-edge.io:8080/swift/v1/AUTH_d932d738da434a1588f36a12339b84c0/ipxe/ipxe-web.qcow2

This image is meant to be executed on the KVM hypervisor

## Login to the image

There is a default user account and password set for this image. You may login with the following if you didn't use cloud-init to set the credentials to get into the image.

Username: ipxe
Password: ipxe

## Build the image yourself
To build this image yourself, you must have diskimage-builder installed on a linux machine. https://docs.openstack.org/diskimage-builder/latest/user_guide/installation.html

```
git clone https://gitlab.com/fortnebula/ipxe-web.git
cd ipxe-web
source dibenv
disk-image-create -o ipxe-web -t qcow2 ${DISTRO} cloud-init-datasources vm devuser ipxe-web
```

## Build the image yourself using the fortnebula dibtools container - BETA
This method leverages the fortnebula dibtools container built here - https://gitlab.com/fortnebula/dibtools
The only requirement is that your system has a docker container compatible engine, you may also use podman.
* This method will mount a volume to the builds directory in your current folder and then mount that volume. 
* This container needs to be privileged to function correctly.
* This tool is still beta, so feedback is welcome

```
mkdir -p builds
docker run -it --privileged -v $(pwd)/builds:/builds registry.gitlab.com/fortnebula/dibtools /bin/bash
git clone https://gitlab.com/fortnebula/ipxe-web.git
cd ipxe-web
source dibenv
disk-image-create -o /builds/ipxe-web -t qcow2 ${DISTRO} cloud-init-datasources vm devuser ipxe-web
exit
ls -al builds
```
## Tooling for auto-configuration on nocloud hosts

If you are using something like vmware or regular kvm there are no cloud metadata services. However you can use this app to produce a config-drive to auto
configure your instance. Check out
https://clouddrive.apps.fortnebula.com/

You will need to hand the config drive tool some parameters you want to have configured in your instance, so please brush up on cloud-init here.

https://cloudinit.readthedocs.io/en/latest/

## Feedback

This image is still very much a work in progress, so please report issues and let us know what is wrong with it. 
