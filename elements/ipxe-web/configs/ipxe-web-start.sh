#!/bin/bash
pkill apache2
mkdir -p /var/cache/ipxe-build /var/run/ipxe-build /var/tmp/ipxe-build
cd /var/tmp/
rm -rf ipxe
git clone https://github.com/ipxe/ipxe.git
touch /var/run/ipxe-build/ipxe-build-cache.lock
chown -R www-data:www-data /var/run/ipxe-build/ipxe-build-cache.lock /var/cache/ipxe-build /var/run/ipxe-build /var/tmp/ipxe-build /var/tmp/ipxe
apachectl -DFOREGROUND &